jQuery(document).ready(function($){
  $('#moreOptions').on('click', function() {
    $('#searchOptions').toggleClass('open');
    if ($('#searchOptions').hasClass('open')) {
      $('#moreOptions').text('Less Options');
    } else {
      $('#moreOptions').text('More Options');
    }
  });
  $('#toggleNew').on('click', function() {
    $('#new').toggleClass('open');
    if ($('#new').hasClass('open')) {
      $('#toggleNew').text('Close');
    } else {
      $('#toggleNew').text('Open');
    }
  });
});
