@extends('layouts/app')

@section('content')
  <main>
    <header class="small-12 large-hidden">
      <h1>Project Samples</h1>
    </header>
    @if(isset($user))
      @if ($user->role == 'FPStudent')
        <section>
          @if(isset ($samples))
          <h2>All Samples</h2>
            @forelse ($samples as $sample)
              <article>
                <h3>{{  $sample->title  }}</h3>
                <p>{{  $sample->desc  }}</p>
                <a href="/samples/show/{{$sample->id}}"><button class="skipbtn" type="button" name="button">Take a Look</button></a>
              </article>
            @empty
              <p style="text-align: center;">No Samples found matching your Search</p>
            @endforelse
          @endif
        </section>
      @else
        <section id="search" class="toggle column small-12 large-12">
          <form class="" role="search" action="/samples" accept-charset="UTF-8" method="get">
            {{ csrf_field() }}

            <input type="text" name="kwSearch" placeholder="Find a Sample..." class="column small-10 large-10">
            <button type="submit" name="filter" value="none" id="searchBtn" class="column small-hidden large-2"><p ><i class="fa fa-search fa-1x" aria-hidden="true" ></i>Search</p></button>
            <button type="submit" name="filter" value="none" id="searchBtn" class="column small-2 large-hidden"><p ><i class="fa fa-search fa-1x" aria-hidden="true" ></i></p></button> 
            <a href="#" id="moreOptions">More Options</a>
            <div id="searchOptions">
              <label for="categories">Categories</label>
              {{-- Bring in all Categories from table. --}}
              @foreach($allCategories as $category)
                <input type="checkbox" name="categories[]" value="{{$category->id}}">{{$category->name}}
              @endforeach


              </form>
              <h2>Filter</h2>
              <form class="" action="/samples" method="get">
                {{ csrf_field() }}

                <input type="hidden" name="currentSamples" value="{{$searchSamples}}">

                <button type="submit"  name="filter" value="newest">Newest First</button>
                <button type="submit" name="filter" value="oldest">Oldest First</button>
              </form>
            </div>

        </section>
        <section class="column small-12 large-12">
          @if(isset ($searchSamples))
            <h2>Samples</h2>
            @foreach ($searchSamples as $sample)
              <a href="/samples/{{$sample->id}}">
                <article class="sampleListing column small-12 large-12">

                  <h3 class="column small-12 large-12">{{  $sample->title  }}</h3>
                  <img src="img/sampleImg.jpg" alt="Stock image for Sample" class="column small-4 large-4">
                  <p class="column small-8 large-8 desc">{{  $sample->desc  }}</p>
                  <p class="column small-8 large-8 category">Category: {{$sample->category->name}}</p>

                  <p class="column small-12 large-12 highlight">Take a Look</p>


                </article>
              </a>
            @endforeach
          @endif
        </section>
        <section>
              <!-- Recently Added -->

        </section>
      @endif
    @endif
  </main>
@endsection
