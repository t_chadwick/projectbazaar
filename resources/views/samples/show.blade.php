@extends('layouts/app')

@section('content')
  <main>
    <header>
      <h1>{{ $sample->title }}</h1>
    </header>
    <section>
      <p>{{ $sample->desc }}</p>
      <!--Sample Content-->
      {{-- The sample will be uploaded as an image (or pdf) to the sample's individual folder --}}
      <figure>
        <img class="sample-img" src="img/samples/{{$sample->id}}/img1.jpg" alt="">
        <figcaption>{{$sample->title}}</figcaption>
      </figure>
    </section>
  </main>
@endsection
