<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Project Bazaar</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/css/foundation.min.css">

    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>


    {{-- Adding these two lines provides a ui for text areas--}}
    {{-- URL: https://www.tinymce.com/ --}}
    {{-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script> --}}
    <script src="/js/tinymce/js/tinymce/tinymce.min.js"></script>
    {{-- The forced root block bit stops it from adding a p tag to the text --}}
    <script>tinymce.init({forced_root_block : "", selector:'textarea'});</script>



    {{-- Font-Awesome --}}
    <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
</head>
<body id="app" class="row">
    <header>
      <nav class="column large-12 small-12">
        <div class="column small-hidden large-4">
          <h1>
          @if(strpos(url()->current(), '/home'))
            <img src="img/final_logo.png" alt="Project Bazaar Logo" width="7.5%">Home
          @elseif (strpos(url()->current(), '/projex'))
            <img src="/img/final_projexLogo.png" alt="Projex" width="7.5%">
            Projex
          @elseif (strpos(url()->current(), '/ideas'))
            Project Ideas
          @elseif (strpos(url()->current(), '/samples'))
            Project Samples
          @elseif (strpos(url()->current(), '/admin'))
            Project Bazaar Admin
          @else
            No Title
          @endif
        </h1>
      </div>
        <ul id="navLinks" class="column small-12 large-6">
            <li id="nav-account" class="column small-3 large-3">
              <a href="/home">
                <i class="fa fa-user fa-3x" aria-hidden="true" ></i>@if (isset ($user))
                    {{$user->fname}}
                @else
                  Guest
                @endif
              </a>
            </li>
            <li id="nav-projex" class="column small-3 large-3">
              <a href="/projex">
                <img src="/img/final_projexLogo.png" alt="Projex" width="50px"><p>Blog</p>
              </a>
            </li>
          @if(isset($user))
            @if ($user->role->name == 'Admin' || $user->role->name == 'Tutor' || $user->role->name == 'PStudent')
            <li id="nav-ideas" class="column small-3 large-3">
              <a href="/ideas">
                <i class="fa fa-lightbulb-o fa-3x" aria-hidden="true"></i>Ideas
              </a>
            </li>
            @if ($user->role->name == 'FPStudent' || $user->role->name == 'Admin' || $user->role->name == 'Tutor' || $user->role->name == 'PStudent')
                <li id="nav-samples" class="column small-3 large-3">
                  <a href="/samples">
                    <i class="fa fa-file-text fa-3x" aria-hidden="true"></i>Samples
                  </a>
                </li>
            @endif
            @endif
          @endif
        </ul>
        @if(isset ($user))
          <div class="column small-hidden large-2" id="logout">
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                <button type="button" name="button" class="skipbtn">Logout</button>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </div>
        @else
          <div class="column small-hidden large-2" id="logout">
            <a href="{{ url('/login') }}"><button class="skipbtn" type="button" name="login">Login</button></a>
          </div>
        @endif
      </nav>
    </header>
    @yield('content')
    <!-- Scripts -->
    <script src="/js/app.js"></script>
  </body>
</html>
