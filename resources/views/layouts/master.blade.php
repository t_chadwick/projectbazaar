<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Project Bazaar</title>
        <!-- Styles -->
        <link rel="stylesheet" href="/css/foundation.min.css">
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body class="row">
      @yield('content')
    </body>
</html>
