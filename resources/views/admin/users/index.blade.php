@extends('layouts.app')


@section('content')
  <main>
    <header>
      <h1>All Users</h1>
    </header>
    <a href="/admin"><button type="button" name="button" class="skipbtn"><i class="fa fa-chevron-left fa-1x" aria-hidden="true" ></i> Back</button></a>
    <section class="column small-12 large-12">
      @if (isset($allUsers))
        @foreach ($allUsers as $user)
          <article class="column small-12 large-4">
            <h3>{{$user->id}}: {{$user->fname}} {{$user->sname}}</h3>
            <p>Email: {{$user->email}}</p>
            <p>Role: {{$user->role->name}}</p>
            <a href="/admin/users/{{$user->id}}/edit"><button type="button" name="button"class="skipbtn"><i class="fa fa-wrench fa-1x" aria-hidden="true" ></i> Manage</button></a>
          </article>
        @endforeach
      @endif
    </section>
  </main>
@endsection
