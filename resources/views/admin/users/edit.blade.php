@extends('layouts/app')

@section('content')
  <main>
    <header>
      <h1>Edit User</h1>
    </header>
    <section>
      <a href="/admin/users"><button type="button" name="button" class="skipbtn"><i class="fa fa-chevron-left fa-1x" aria-hidden="true" ></i> Back</button></a>
      <form class="" action="{{ action('UserController@update', [ 'id' => $user2->id]) }}" method="post">
        <input type="hidden" name="_method" value="PATCH">
        {{ csrf_field() }}
        <label for="fname">First Name</label>
        <input type="text" name="fname" value="{{$user2->fname}}">
        <label for="sname">Last Name</label>
        <input type="text" name="sname" value="{{$user2->sname}}">
        <label for="email">Email</label>
        <input type="email" name="email" value="{{$user2->email}}">
        <label for="role">Role</label>
        @foreach ($roles as $role)
          <input type="radio" name="role_id" @if ($role->id == $user2->role->id)
            checked="checked"
          @endif value="{{$role->id}}">{{$role->name}}
        @endforeach
        <button type="submit" name="" class="saveBtn"><i class="fa fa-floppy-o fa-1x" aria-hidden="true" ></i> Save Changes</button>
      </form>
      <form class="" action="/admin/users/{{$user2->id}}" method="post">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{$user2->id}}">
        <button type="submit" name="delete" class="deleteBtn"><i class="fa fa-trash fa-1x" aria-hidden="true" ></i> Delete</button>
      </form>
    </section>
  </main>
@endsection
