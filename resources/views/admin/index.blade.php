@extends('layouts.app')

{{-- Admin Home Page --}}

@section('content')
  <main>
    <header class="column small-12 large-hidden">
      <h1>Project Bazaar Admin</h1>
    </header>
    <div class="column small-6 large-hidden">
      <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
          <button type="button" name="button" class="skipbtn">Logout</button>
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>
    </div>
    <section class="column small-12 large-12">
      <a href="/admin/users"><button type="button" name="button"><i class="fa fa-wrench fa-1x" aria-hidden="true" ></i> Manage Users</button></a>
      <a href="/ideas"><button type="button" name="button"><i class="fa fa-wrench fa-1x" aria-hidden="true" ></i> Manage Ideas</button></a>
      <a href="/samples"><button type="button" name="button"><i class="fa fa-wrench fa-1x" aria-hidden="true" ></i> Manage Samples</button></a>
      <a href="/projex"><button type="button" name="button"><i class="fa fa-wrench fa-1x" aria-hidden="true" ></i> Manage Articles</button></a>
    </section>
  </main>
@endsection
