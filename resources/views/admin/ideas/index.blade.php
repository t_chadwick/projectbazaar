@extends('layouts.app')


{{-- Admin Idea Index Page --}}

@section('content')
  <main>
    <header>
      <h1>All Ideas</h1>
    </header>
    <a href="/admin"><button type="button" name="button" class="skipbtn"><i class="fa fa-chevron-left fa-1x" aria-hidden="true" ></i> Back</button></a>
    <section class="column small-12 large-12">
      @if (isset($allIdeas))
        @foreach ($allIdeas as $idea)
          <article class="column small-12 large-4">
            <h3>{{$idea->id}}: {{$idea->title}}</h3>
            <p>Description: {{$idea->desc}}</p>
            <p>Tutor: {{$idea->user->fname}} {{$idea->user->sname}}</p>
            <a href="/ideas/{{$idea->id}}/edit"><button type="button" name="button"class="skipbtn"><i class="fa fa-wrench fa-1x" aria-hidden="true" ></i> Manage</button></a>
          </article>
        @endforeach
      @endif
    </section>
  </main>
@endsection
