@extends('layouts.app')

{{-- Admin Article Index Page --}}

@section('content')
  <main>
    <header>
      <h1>All Articles</h1>
    </header>
    <a href="/admin"><button type="button" name="button" class="skipbtn"><i class="fa fa-chevron-left fa-1x" aria-hidden="true" ></i> Back</button></a>
    <a href="/admin/articles/create"><button type="button" name="button" class="skipbtn"><i class="fa fa-plus fa-1x" aria-hidden="true"></i> New Article</button></a>
    <section class="column small-12 large-12">
      @if (isset($allArticles))
        @foreach ($allArticles as $article)
          <article class="column small-12 large-4">
            <h3>{{$article->id}}: {{$article->title}}</h3>
            <p>Description:</p>{{$article->desc}}
            <p>Category: {{$article->category->name}}</p>
            <p>Author: {{$article->author}}</p>
            <a href="/projex/{{$article->id}}/edit"><button type="button" name="button"class="skipbtn"><i class="fa fa-wrench fa-1x" aria-hidden="true" ></i> Manage</button></a>
          </article>
        @endforeach
      @endif
    </section>
  </main>
@endsection
