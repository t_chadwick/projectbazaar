@extends('layouts.app')

{{-- Article Creation Page --}}

@section('content')
  <main>
    <header>
      <h1>New Article</h1>
    </header>
    <a href="/projex"><button type="button" name="button" class="skipbtn"><i class="fa fa-chevron-left fa-1x" aria-hidden="true" ></i> Back</button></a>

    <section class="column small-12 large-12">
      <form class="" action="{{ action('ProjexController@store')}}" method="post">
        {{ csrf_field() }}
        <label for="title">Title</label>
        <input type="text" name="title" value="">
        <label for="desc">Description</label>
        <textarea name="desc" rows="8" cols="30"></textarea>
        <label for="category">Category</label>
        <select class="" name="category">
          @foreach ($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}
          @endforeach
        </select>
        <button type="submit" name="button" class="saveBtn"><i class="fa fa-floppy-o fa-1x" aria-hidden="true" ></i>Create</button>
      </form>
    </section>
  </main>
@endsection
