@extends('layouts.app')


{{-- Article Edit Page --}}

@section('content')
  <main>
    <header>
      <h1>Edit Article</h1>
    </header>
    <a href="/projex"><button type="button" name="button" class="skipbtn"><i class="fa fa-chevron-left fa-1x" aria-hidden="true" ></i> Back</button></a>

    <section class="column small-12 large-12">
      <form class="" action="{{ action('ProjexController@update', [ 'id' => $article->id]) }}" method="post">
        <button type="submit" name="button" class="saveBtn"><i class="fa fa-floppy-o fa-1x" aria-hidden="true" ></i> Save Changes</button>
        <input type="hidden" name="_method" value="PATCH">
        {{ csrf_field() }}
        <label for="title">Title</label>
        <input type="text" name="title" value="{{$article->title}}">
        <label for="desc">Description</label>
        <textarea name="desc" rows="8" cols="30">{{$article->desc}}</textarea>
      </form>
      @foreach ($sections as $section)
        <form class="" action="{{ action('SectionController@update', [ 'id' => $section->id]) }}" method="post">
          <input type="hidden" name="_method" value="PATCH">
          {{ csrf_field() }}
          <input type="hidden" name="article_id" value="{{$article->id}}">
          <label for="content" class="column small-12 large-12">Section</label>
          <textarea name="content" rows="20" cols="30" class="">{{$section->content}}</textarea>
          <button type="submit" name="button" class="saveBtn"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true" ></i> Update Section</button>
        </form>
        <form class="" action="/sections/{{$section->id}}" method="post">
          {{ method_field('DELETE') }}
          {{ csrf_field() }}
          <input type="hidden" name="article_id" value="{{$article->id}}">
          <button type="submit" name="delete" class="deleteBtn"><i class="fa fa-trash fa-1x" aria-hidden="true" ></i> Delete Section</button>
        </form>
      @endforeach
      <form class="" action="{{ action('SectionController@store')}}" method="post">
      {{ csrf_field() }}
      <input type="hidden" name="article_id" value="{{$article->id}}">
      <button type="submit" name="button" class="skipbtn"><i class="fa fa-plus fa-1x" aria-hidden="true" ></i> Add Section</button>
      </form>
      <form class="" action="/projex/{{$article->id}}" method="post">
          {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <input type="hidden" name="article_id" value="{{$article->id}}">
        <button type="submit" name="delete" class="deleteBtn"><i class="fa fa-trash fa-1x" aria-hidden="true" ></i> Delete Article</button>
      </form>
    </section>
  </main>
@endsection
