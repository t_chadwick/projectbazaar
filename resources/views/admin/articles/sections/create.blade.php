@extends('layouts.app')

{{-- Article Section Creation Page --}}

@section('content')
  <main>
    <header>
      <h1>New Article Section</h1>
    </header>
    <a href="/admin/articles/{{$article->id}}/edit"><button type="button" name="button" class="skipbtn">Back</button></a>
    <section class="column small-12 large-12">
      <form class="" action="{{ action('SectionController@create', [ 'id' => $article->id]) }}" method="post">
        {{ csrf_field() }}
        <label for="content">Content</label>
        <textarea name="content" rows="8" cols="80"></textarea>
        <button type="submit" name="button">Add</button>
      </form>
    </section>
  </main>
@endsection
