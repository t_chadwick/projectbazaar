@extends('layouts.app')

{{-- Admin Edit Sample Page --}}

@section('content')
  <main>
    <header>
      <h1>Edit Sample</h1>
    </header>
    <a href="/samples"><button type="button" name="button" class="skipbtn"><i class="fa fa-chevron-left fa-1x" aria-hidden="true" ></i> Back</button></a>
    <section class="column small-12 large-12">
      <form class="" action="{{ action('SamplesController@update', [ 'id' => $sample->id]) }}" method="post">
        <input type="hidden" name="_method" value="PATCH">
        {{ csrf_field() }}
        <label for="title">Title</label>
        <input type="text" name="title" value="{{$sample->title}}">
        <label for="desc">Description</label>
        <textarea name="desc" rows="8" cols="30">{{$sample->desc}}</textarea>
        <label for="category">Category</label>
        @foreach ($categories as $category)
          <input type="radio" name="category" @if ($category->id == $sample->category->id)
            checked="checked"
          @endif value="{{$category->id}}">{{$category->name}}
        @endforeach
        <label for="fps_view">Visible to Future Project Student?</label>
        @if($sample->fps_view == 1)
          <input type="radio" name="fps_view" value="1" checked="checked">Yes
          <input type="radio" name="fps_view" value="0">No
        @elseif($sample->fps_view == 0)
          <input type="radio" name="fps_view" value="1">Yes
          <input type="radio" name="fps_view" value="0" checked="checked">No
        @endif
        <button type="submit" name="button" class="saveBtn"><i class="fa fa-floppy-o fa-1x" aria-hidden="true" ></i> Save Changes</button>
      </form>
      <form class="" action="/samples/{{$sample->id}}" method="post">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <button type="submit" name="delete" class="deleteBtn"><i class="fa fa-trash fa-1x" aria-hidden="true" ></i> Delete Article</button>
      </form>
    </section>
  </main>
@endsection
