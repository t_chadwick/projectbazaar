@extends('layouts.app')

{{-- Admin Samples Index Page --}}

@section('content')
  <main>
    <header>
      <h1>All Samples</h1>
    </header>
    <a href="/admin"><button type="button" name="button" class="skipbtn"><i class="fa fa-chevron-left fa-1x" aria-hidden="true" ></i> Back</button></a>
    <a href="/admin/samples/create"><button type="button" name="button" class="skipbtn"><i class="fa fa-plus fa-1x" aria-hidden="true"></i> New Sample</button></a>
    <section class="column small-12 large-12">
      @if (isset($allSamples))
        @foreach ($allSamples as $sample)
          <article class="column small-12 large-4">
            <h3>{{$sample->id}}: {{$sample->title}}</h3>
            <p>Description: {{$sample->desc}}</p>
            <p>Category: {{$sample->category->name}}</p>
            <p>Visible to Future Project Student? : @if($sample->fps_view == 1) Yes @else No @endif</p>

            <a href="/samples/{{$sample->id}}/edit"><button type="button" name="button"class="skipbtn"><i class="fa fa-wrench fa-1x" aria-hidden="true" ></i> Manage</button></a>
          </article>
        @endforeach
      @endif
    </section>
  </main>
@endsection
