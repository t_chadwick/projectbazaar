@extends('layouts/master')

@section('content')
  <h1>Student Sign In</h1>
  {{Form::open(array('url' => '/login/student/signin', 'method' => 'POST'))}}
    <div class="form-group">
        {!! Form::label('email', 'Email:', ['class' => "control-label"]) !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        {!! Form::submit('Sign In') !!}
    </div>
  {{Form::close()}}
  <form class="form-group" action="/login/student/signin" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <label class="control-label" for="type">Email: </label>
      <input class="form-control" type="email" name="email" value="">
      <button type="submit">
          Sign In
      </button>
  </form>
  <h3>Need an Account? Fill in the form below.</h3>
  <h1>Sign Up</h1>
  <form class="form-group" action="" method="">
      <label class="control-label" for="fname">First Name</label>
      <input class="form-control" type="textarea" name="fname" value="">
      <label class="control-label" for="sname">Surname</label>
      <input class="form-control" type="textarea" name="sname" value="">
      <label class="control-label" for="email">E-mail</label>
      <input class="form-control" type="email" name="email" value="">
      <label class="control-label" for="type">Type of Student</label>
      <input class="form-control" type="radio" name="type" value="ps">Project Student
      <input class="form-control" type="radio" name="type" value="fps">Future Project Student
      <button type="submit">
          Sign Up
      </button>
  </form>
