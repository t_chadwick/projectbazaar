@extends('layouts/master')

@section('content')

  {{-- Landing Page --}}


  <div id="landingContent" class="column small-12 large-12">
    <div class="title centered column small-12 large-12">
      <img class="landLogo" src="/img/final_logoCropped.png" alt="Project Bazaar" width="15%" style="max-width: 250px;">
      <h1>Project Bazaar</h1>
    </div>
    <div id="landingLinks" class="centered column small-12 large-12">
      @if (Route::has('login'))
        @if (Auth::check())
          <p class="column small-12 large-12">Logged In as {{Auth::user()->fname}} {{Auth::user()->sname}}</p>
          <a class="column small-12 large-12" href="{{ url('/home') }}"><button class="skipbtn" type="button" name="button">Continue</button></a>
          <p class="column small-12 large-12">Not {{Auth::user()->fname}}? <a class="column small-12 large-12" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();"><button class="skipbtn" type="button" name="button">Logout</button></a></p>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
        @else
          <div>
            <div class="column small-hidden large-3">
              <p></p>
            </div>
            <div class="column small-6 large-3">
              <a href="{{ url('/login') }}"><button class="" type="button" name="login">Login</button></a>
            </div>
            <div class="column small-6 large-3">
              <a href="{{ url('/register') }}"><button class="" type="button" name="register">Register</button></a>
            </div>
            <div class="column small-hidden large-3">
              <p></p>
            </div>
            <div class="column small-12 large-12">
              <a href="{{ url('/projex')}}"><button class="skipbtn" type="button" name="button">Continue as Guest</button></a>
            </div>
          </div>
        @endif
      @endif
    </div>
  </div>
@endsection
