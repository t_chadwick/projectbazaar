@extends('layouts.app')


{{-- Home Page --}}

@section('content')
<main class="column small-12 large-12">
  <header class="column small-12 large-hidden">
    <h1 class="column small-6 large-10"><img src="img/final_logo.png" alt="Project Bazaar Logo" width="25%"> Home</h1>
    <div class="column small-6 large-hidden">
      <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
          <button type="button" name="button" class="skipbtn">Logout</button>
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>
    </div>
  </header>
  <article >
    <section class="toggle column small-12 large-12" >
      <h3 class="column small-6 large-6">Whats New</h3>
      <button type="button" name="button" class="column small-6 large-2" id="toggleNew">Open</button>
      <div id="new" >
        @if($user->role->name == 'Tutor' | $user->role->name == 'PStudent')
          <h3 class="column small-12 large-12">New Project Ideas:</h3>
          @if (isset($latestIdea))
            <article class="column small-12 large-12">
              <h4>{{$latestIdea->title}}</h4>
              <p>Field: {{$latestIdea->category->name}}</p>
              <p>{{$latestIdea->desc}}</p>
              <a href="/ideas/{{$latestIdea->id}}"><button type="button" name="button" class="skipbtn">Find Out More</button></a>
            </article>
          @else
            <p class="column small-12 large-12">No New Project Ideas</p>
          @endif
        @endif
        <h3 class="column small-12 large-12">Projex Blog: Latest Post</h3>
        @if (isset($latestPost))
          <article class="column small-12 large-12">
            <h4>{{$latestPost->title}}</h4>
            <p>Category: {{$latestPost->category->name}}</p>
            <p>{{$latestPost->desc}}</p>
            <a href="/projex/{{$latestPost->id}}"><button type="button" name="button" class="skipbtn">Read More</button></a>
          </article>
        @else
          <p class="column small-12 large-12">No New Posts</p>
        @endif
      </div>
    </section>
    <section>
      @if ($user->role->name == 'Tutor')
        <h1 class="">Your Project Ideas</h1>
        <a href="/ideas/create"><button type="button" name="button" class="skipbtn"><i class="fa fa-plus fa-1x" aria-hidden="true"></i> New Idea</button></a>
        @if(isset ($ideas))
          @foreach ($ideas as $idea)
            <article class="">
              <h3>{{$idea->title}}</h3>
              <p>{{$idea->desc}}</p>
              <a href="/ideas/{{$idea->id}}"><button type="button" name="button" class="skipbtn"><i class="fa fa-eye fa-1x" aria-hidden="true" ></i> View</button></a>
              <a href="/ideas/{{$idea->id}}/edit"><button type="button" name="button" class="skipbtn"><i class="fa fa-wrench fa-1x" aria-hidden="true" ></i> Manage</button></a>
            </article>
          @endforeach
        @endif
      @elseif($user->role->name == 'PStudent')
        <h3>Your Applications</h3>
        @if(isset ($application))
          <h3>{{$application->idea->title}}</h3>
          <p>{{$application->idea->desc}}</p>
          <p>Status: {{$application->status}}</p>
          @if($application->status == 'Pending')
            <form class="" action="/application/{{$application->id}}" method="post">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <input type="hidden" name="application_id" value="{{$application->id}}">
              <input type="hidden" name="idea_id" value="{{$application->idea_id}}">

              <button type="submit" name="cancel" class="deleteBtn">Cancel Application</button>
            </form>
          @endif
        @endif
      @endif
    </section>
  </article>
</main>
@endsection
