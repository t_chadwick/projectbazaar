@extends('layouts/app')

@section('content')
  <main>
    <header>
      <h1>{{ $article->title }}</h1>
    </header>
    <section>
      @foreach ($sections as $section)
          {{-- Changing this to have the '!!' makes the page render the content as HTML and not a string. --}}
          {!! $section->content !!}
      @endforeach
    </section>
    <section>
      <!--Reccommended based on this article?-->
    </section>
  </main>


@endsection
