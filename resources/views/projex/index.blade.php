@extends('layouts/app')

@section('content')
  <main class="column small-12 large-12">
    <header class="small-12 large-hidden">
      <img src="/img/final_projexLogo.png" alt="Projex" width="10%">
      <h1 class="column small-10">Welcome to Projex</h1>
    </header>
    <section id="search" class="toggle column small-12 large-12">
      <form class="" role="search" action="/projex" accept-charset="UTF-8" method="get">
        {{ csrf_field() }}

        <input type="text" name="kwSearch" placeholder="Find an Article..." class="column small-10 large-10">
        <button type="submit" name="filter" value="none" id="searchBtn" class="column small-hidden large-2"><p ><i class="fa fa-search fa-1x" aria-hidden="true" ></i>Search</p></button>
        <button type="submit" name="filter" value="none" id="searchBtn" class="column small-2 large-hidden"><p ><i class="fa fa-search fa-1x" aria-hidden="true" ></i></p></button>
        <a href="#" id="moreOptions">More Options</a>
        <div id="searchOptions">
          <label for="categories">Categories</label>
          {{-- Bring in all Categories from table. --}}
          @foreach($allCategories as $category)
            <input type="checkbox" name="categories[]" value="{{$category->id}}">{{$category->name}}
          @endforeach


        </form>
        <h2>Filter</h2>
        <form class="" action="/projex" method="get">
          {{ csrf_field() }}

          <input type="hidden" name="currentArticles" value="{{$searchArticles}}">

          <button type="submit"  name="filter" value="newest">Newest First</button>
          <button type="submit" name="filter" value="oldest">Oldest First</button>
        </form>
        </div>
    </section>


    <section class="column small-12 large-12">
      @if(isset ($searchArticles))
        <h2>Articles</h2>
        @forelse ($searchArticles as $article)
          <a href="/projex/{{$article->id}}">
            <article class="articleListing column small-12 large-12">
              <img src="img/articleImg.jpg" alt="Stock Image for Blog Article" class="column small-4 large-2">
              <h3 class="column small-8 large-10"> {{  $article->title  }}</h3>
              <p class="column small-8 large-10 category"> {{$article->category->name}}</p>
              <p class="column small-12 large-12 desc">{{  $article->desc  }}</p>
              <p class="column small-4 large-2 highlight">Read More</p>
            </article>
          </a>
        @empty
          <p style="text-align: center;">No Articles found matching your Search</p>
        @endforelse
      @endif
    </section>
  </main>
@endsection
