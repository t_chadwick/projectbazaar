@extends('layouts.master')

{{-- Login Page --}}

@section('content')
  <section class="column small-12">
      <div class="column small-12 large-12">
            <div class="column small-12 large-12">
                <div class="column small-hidden large-4">
                  <p></p>
                </div>
                <div class="column small-12 large-4">
                    <form class="" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="centered column small-12 large-12">
                            <img class="landLogo-cut20-top" src="/img/final_logoCropped.png" alt="Project Bazaar" width="10%">
                        </div>
                          <div class="centered"><h3>Login</h3></div>

                        <div class="column small-12 large-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="">E-Mail Address</label>

                            <div class="column small-12 large-12">
                                <input type="email" class="" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="column small-12 large-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="">Password</label>

                            <div class="column small-12 large-12">
                                <input type="password" class="" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="column small-12 large-12">
                            <input class="column small-1 large-1" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="column small-10 large-10">Remember Me</label>
                        </div>

                        <div class="centered">
                            <div class="">
                                <button type="submit" class="">
                                    Login
                                </button>

                                <a class="" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="column small-hidden large-4">
                  <p></p>
                </div>
                <div class="centered column small-12 large-12">
                    <h3 class="text-white">Need an account?</h3>
                </div>
                <div class="centered column small-12 large-12">
                  <a href="/register  "><button class="skipbtn" type="button" name="button">Register Here</button></a>
                </div>
            </div>
      </div>
  </section>
@endsection
