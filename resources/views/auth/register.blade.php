@extends('layouts.master')

{{-- Registration Page --}}

@section('content')
  <section class="container">
        <form class="" role="form" method="POST" action="/register">
            {{ csrf_field() }}

            <div class="centered"><h3>Register</h3></div>

            <div class="{{ $errors->has('fname') ? ' has-error' : '' }}">
                <label for="fname" class="column small-12 large-12">First Name</label>

                <div class="column small-12 large-12">
                    <input id="fname" type="text" class="" name="fname" value="{{ old('fname') }}" required autofocus>

                    @if ($errors->has('fname'))
                        <span class="">
                            <strong>{{ $errors->first('fname') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="{{ $errors->has('sname') ? ' has-error' : '' }}">
                <label for="sname" class="column small-12 large-12">Surname</label>

                <div class="column small-12 large-12">
                    <input id="sname" type="text" class="" name="sname" value="{{ old('sname') }}" required autofocus>

                    @if ($errors->has('sname'))
                        <span class="">
                            <strong>{{ $errors->first('sname') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="column small-12 large-12">E-Mail Address</label>

                <div class="column small-12 large-12">
                    <input id="email" type="email" class="" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="column small-12 large-12">Password</label>

                <div class="column small-12 large-12">
                    <input id="password" type="password" class="" name="password" required>

                    @if ($errors->has('password'))
                        <span class="">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <label for="password-confirm" class="column small-12 large-12">Confirm Password</label>
            <input id="password-confirm" type="password" class="column small-12 large-12" name="password_confirmation" required>

            <label class="" for="role">Who Are You?</label>
            <div class="column small-12 large-12 radio-button">
              <input id="role" class="column small-1 large-1" type="radio" name="role" value="2" required><p class="column small-11 large-11"><span></span>Tutor</p>
              <input id="role" class="column small-1 large-1" type="radio" name="role" value="4" required><p class="column small-11 large-11" required><span></span>Future Project Student</p>
              <input id="role" class="column small-1 large-1" type="radio" name="role" value="3" required><p class="column small-11 large-11"><span></span>Project Student</p>
            </div>


            <button type="submit" class="">
                Register
            </button>
        </form>
  </section>
@endsection
