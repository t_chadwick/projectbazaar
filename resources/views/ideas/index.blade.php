@extends('layouts/app')

{{-- Ideas Index Page --}}

@section('content')
  <main>
    <header class="small-12 large-hidden">
      <h1>Project Ideas</h1>
    </header>
    <section id="search" class="toggle column small-12 large-12">
      <form class="" role="search" action="/ideas" accept-charset="UTF-8" method="get">
        {{ csrf_field() }}

        <input type="text" name="kwSearch" placeholder="Find an Idea..." class="column small-10 large-10">
        <button type="submit" name="filter" value="none" id="searchBtn" class="column small-hidden large-2"><p ><i class="fa fa-search fa-1x" aria-hidden="true" ></i>Search</p></button>
        <button type="submit" name="filter" value="none" id="searchBtn" class="column small-2 large-hidden"><p ><i class="fa fa-search fa-1x" aria-hidden="true" ></i></p></button>
        <a href="#" id="moreOptions">More Options</a>
        <div id="searchOptions">
          <label for="categories">Fields</label>
          {{-- Bring in all Categories from table. --}}
          @foreach($allCategories as $category)
            <input type="checkbox" name="categories[]" value="{{$category->id}}">{{$category->name}}
          @endforeach
        </form>
        <h2>Filter</h2>
        <form class="" action="/ideas" method="get">
          {{ csrf_field() }}

          <input type="hidden" name="currentIdeas" value="{{$searchIdeas}}">

          <button type="submit"  name="filter" value="newest">Newest First</button>
          <button type="submit" name="filter" value="oldest">Oldest First</button>
        </form>
      </div>
    </section>
    <section class="column small-12 large-12">
      @if(isset ($searchIdeas))
        <h2>Ideas</h2>
        @forelse ($searchIdeas as $idea)
          <a href="/ideas/{{$idea->id}}">
            <article class="ideaListing column small-12 large-12">
              <h3 class="column small-12 large-12">{{  $idea->title  }}</h3>
              <img src="img/ideaImage.jpg" alt="Stock Image for Idea Listing" class="column small-4 large-2">
              <p class="column small-6 large-10 desc">{{  $idea->desc  }}</p>
              <p class="column small-6 large-10 category">Field: {{$idea->category->name}}</p>
              <p class="column small-6 large-2 highlight">Find Out More </p>
            </article>
          </a>
        @empty
          <p style="text-align: center;">No Ideas found matching your Search</p>
        @endforelse
      @endif

    </section>
    <section>
          <!-- Recently Added -->

    </section>
  </main>


@endsection
