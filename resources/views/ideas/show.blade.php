@extends('layouts/app')

{{-- Individual Idea Page --}}

@section('content')
  <main>
    <header>
      <h1>{{ $idea->title }}</h1>
    </header>
    <article>
      <section>
        <p>Description: {{ $idea->desc }}</p>
        <p>Tutor: {{ $idea->user->fname }} {{ $idea->user->sname }}</p>
        <p>Email: {{ $idea->user->email }}</p>
      </section>
      <section class="centered">
        <form class="" action="/application" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="idea_id" value="{{$idea->id}}">
          <button class="skipbtn" type="submit" name="button">Apply</button>
        </form>
      </section>
    </article>
  </main>


@endsection
