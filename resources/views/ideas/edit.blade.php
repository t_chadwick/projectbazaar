@extends('layouts/app')

{{-- Idea Edit Page --}}

@section('content')
  <main>
    <header>
        <h1>Edit Idea</h1>
    </header>
    <a href="/ideas"><button type="button" name="button" class="skipbtn"><i class="fa fa-chevron-left fa-1x" aria-hidden="true" ></i> Back</button></a>
    <article>
      <section>
        <form class="" action="{{ action('IdeasController@update', [ 'id' => $idea->id]) }}" method="post">
          <input type="hidden" name="_method" value="PATCH">
          {{ csrf_field() }}
          {{-- <input type="hidden" name="idea_id" value="{{$idea->id}}"> --}}
        <label for="title">Title</label>
        <input type="text" name="title" value="{{$idea->title}}">
        <label for="desc">Description</label>
        <textarea name="desc" rows="8" cols="30" value="{{ $idea->desc }}">{{ $idea->desc }}</textarea>
      </section>
      <section>
        <button type="submit" name="update" class="saveBtn"><i class="fa fa-floppy-o fa-1x" aria-hidden="true" ></i> Save Changes</button>
        </form>

        <form class="" action="/ideas/{{$idea->id}}" method="post">
            {{ method_field('DELETE') }}
          {{ csrf_field() }}
          <input type="hidden" name="idea_id" value="{{$idea->id}}">
          <button type="submit" name="delete" class="deleteBtn"><i class="fa fa-trash fa-1x" aria-hidden="true" ></i> Delete</button>
        </form>
      </section>
      <section>
        <h2>Applications - {{$idea->applications}} / {{$idea->max_applications}}</h2>

          @foreach ($applications as $application)
            <article class="">
              <h3>{{$application->user->fname}} {{$application->user->sname}}</h3>
              <p>Email: {{$application->user->email}}</p>
              <p>Status: {{$application->status}}</p>
              @if ($application->status == "Pending")
                <form class="" action="{{ action('ApplicationController@update', [ 'id' => $application->id]) }}" method="post">
                  <input type="hidden" name="_method" value="PATCH">
                  {{ csrf_field() }}
                  <button type="submit" name="button" class="saveBtn"><i class="fa fa-check-circle fa-1x" aria-hidden="true" ></i> Accept</button>
                  </form>
                <form class="" action="/application/{{$application->id}}" method="post">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <input type="hidden" name="application_id" value="{{$application->id}}">
                  <input type="hidden" name="idea_id" value="{{$idea->id}}">

                  <button type="submit" name="cancel" class="deleteBtn"><i class="fa fa-times-circle fa-1x" aria-hidden="true" ></i> Decline</button>
                </form>
              @endif
            </article>
          @endforeach

      </section>
  </main>

@endsection
