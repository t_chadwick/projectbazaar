@extends('layouts/app')

{{-- Idea Creation Page --}}

@section('content')
  <h1>Create New Project Idea</h1>
  <form class="" action="/ideas" method="post">
    {{ csrf_field() }}
    <label for="title">Title: </label>
    <input type="text" name="title" value="">
    <label for="desc">Description</label>
    <textarea id="desc" name="desc" rows="8" cols="30" ></textarea>

    <label for="category">Category</label>
    <select  name="category">
      @foreach ($categories as $category)
        <option name="category" value="{{$category->id}}">{{$category->name}}
      @endforeach
    <select>
    <label for="type">Project Type</label>
    <select name="type">
      <option type="radio" name="type" value="Research">Research
      <option type="radio" name="type" value="Development">Development
    </select>
    <label for="max_applications">Maximum Applications</label>
    <input type="number" name="max_applications" value="">
    <button type="submit" name="create">Create</button>
  </form>

@endsection
