<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Login as a Project Student');

$I->SeeRecord('users', [
      'email' => 'rbank@gmail.com',
  ]);

// I start on Landing Page
$I->amOnPage('/');
$I->see('Project Bazaar');

// I Click Login
$I->click('Login');

// Which takes me to the Login page
$I->seeCurrentUrlEquals('/login');
$I->see('Login');

//Fills in Login Form
$I->fillField('email', 'rbank@gmail.com');
$I->fillField('password', 'iamrob');
$I->click('Login', 'button');
// Gets to Dashboard
$I->seeCurrentUrlEquals('/home');
$I->see('Dashboard');
