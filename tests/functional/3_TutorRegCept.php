<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Register as a Tutor');

// I start on Landing Page
$I->amOnPage('/');
$I->see('Project Bazaar');
$I->click('Register');

$I->dontSeeRecord('users', [
      'fname' => 'Test',
      'sname' => 'User',
      'email' => 'tester@gmail.com',
      'role' => 'Tutor',
  ]);

$I->amOnPage('/register');
$I->see('Register');

// I Fill in Registration Form
$I->fillField('fname', 'Test');
$I->fillField('sname', 'User');
$I->fillField('email', 'tester@gmail.com');
$I->fillField('password', 'testing');
$I->fillField('password_confirmation', 'testing');
$I->fillField('role', 'Tutor');
$I->click('Register', 'button');
// Gets to Dashboard
$I->seeCurrentUrlEquals('/home');
$I->see('Dashboard');
