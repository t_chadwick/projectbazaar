<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Test the Search Feature for Project Ideas');

// Test as Admin User (Full Access)
$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// On Ideas Index Page
$I->amOnPage('/ideas');
$I->see('Project Ideas', 'h1');
$I->see('Search', 'h2');

// Check that all Ideas show by default
$I->see('Ideas', 'h2');
$I->see('Idea 1', 'h3');
$I->see('Idea 2', 'h3');
$I->see('Idea 3', 'h3');

// Submit 4 Search Queries and Check Results

// Blank Search
$I->fillField('kwSearch', '');
$I->click('Search', 'button');
// Should display all Ideas
$I->see('Idea 1', 'h3');
$I->see('Idea 2', 'h3');
$I->see('Idea 3', 'h3');

//'1' Search
$I->fillField('kwSearch', '1');
$I->click('Search', 'button');
// Should only display 'Idea 1'
$I->see('Idea 1', 'h3');
// and NOT the other two
$I->dontSee('Idea 2', 'h3');
$I->dontSee('Idea 3', 'h3');

//'Idea 2' Search
$I->fillField('kwSearch', '2');
$I->click('Search', 'button');
// Should only display 'Idea 2'
$I->see('Idea 2', 'h3');
// and NOT the other two
$I->dontSee('Idea 1', 'h3'); // It is showing all?
$I->dontSee('Idea 3', 'h3');

//'Idea' Search
$I->fillField('kwSearch', 'Idea');
$I->click('Search', 'button');
// Should display all Ideas
$I->see('Idea 1', 'h3');
$I->see('Idea 2', 'h3');
$I->see('Idea 3', 'h3');
