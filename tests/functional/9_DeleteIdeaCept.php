<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Delete an Existing Idea.');

// Test as Tutor User
$I->amLoggedAs(['email' => 'wpeace@gmail.com', 'password' => 'longstory']);

//Start on User Dashboard

$I->amOnPage('/home');
$I->see('Your Project Ideas', 'h2');
$I->see('Idea 2', 'h3');
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
$I->click('Manage', 'a[href="/ideas/2/edit"]');

$I->seeCurrentUrlEquals('/ideas/2/edit');

$I->see('Delete', 'button');
$I->click('Delete');

$I->seeCurrentUrlEquals('/home');

$I->dontSee('Idea 2');
