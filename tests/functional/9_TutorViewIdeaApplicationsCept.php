<?php
$I = new FunctionalTester($scenario);
$I->wantTo('View the Applications for one of my Project Ideas.');

// Test as Tutor User
$I->amLoggedAs(['email' => 'wpeace@gmail.com', 'password' => 'longstory']);

//Start on User Dashboard

$I->amOnPage('/home');
$I->see('Your Project Ideas', 'h2');
$I->see('Idea 2', 'h3');
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
$I->seeLink('View', '/ideas/2');
$I->seeLink('Manage', '/ideas/2/edit');
$I->click('Manage', 'a[href="/ideas/2/edit"]');

$I->seeCurrentUrlEquals('/ideas/2/edit');
$I->see('Applications - 2 / 3', 'h2');
$I->see('Chris Cross', 'h3');
$I->see('Email: ccross@gmail.com', 'p');
$I->see('Status: Accepted', 'p');
$I->see('Polly Ester', 'h3');
$I->see('Email: pester@gmail.com', 'p');
$I->see('Status: Pending', 'p');
$I->see('Accept', 'button');
$I->see('Decline', 'button');
