<?php

// I am a General Tester
$I = new FunctionalTester($scenario);
$I->wantTo('Test the Landing Page Navigation');

// I start on Landing Page
$I->amOnPage('/');
$I->see('Project Bazaar');

$I->see('Login');
// I Click Login
$I->click('Login');

// Which takes me to the Login page
$I->seeCurrentUrlEquals('/login');
$I->see('Login');

// I go back to Landing Page
$I->amOnPage('/');

$I->see('Register');
// I Select 'Register'
$I->click('Register');
// Which takes me to the Register page
$I->seeCurrentUrlEquals('/register');
$I->see('Register');
