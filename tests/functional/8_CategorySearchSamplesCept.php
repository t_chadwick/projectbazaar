<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Find all Samples within the Time Management Category');

// Test as Admin User (Full Access)
$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// On Samples Index Page
$I->amOnPage('/samples');
$I->see('Project Samples', 'h1');
$I->see('Search', 'h2');

// Check that all Ideas show by default
$I->see('Samples', 'h2');
$I->see('Literature Review Sample 1', 'h3');
$I->see('Methodology Section Sample 1', 'h3');
$I->see('Gannt Chart Sample 1', 'h3');

// Submit a Search Query and Check Result

//Select 'Time Management Category'
$I->checkOption('#timeManagement'); // This line originally had input[value=Time Management] but this caused a codeception error. (Browser Testing Showed it still functioned)
$I->click('Search', 'button');
// Should only display 'Gannt Chart Sample 1'
$I->see('Gannt Chart Sample 1', 'h3');
// and NOT the other three
$I->dontSee('Literature Review Sample 1', 'h3');
$I->dontSee('Methodology Section Sample 1', 'h3');
$I->dontSee('Proposal Sample', 'h3');
