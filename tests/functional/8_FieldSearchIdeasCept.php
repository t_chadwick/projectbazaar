<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Find all Samples within the Web Development field.');

// Test as Admin User (Full Access)
$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// On Ideas Index Page
$I->amOnPage('/ideas');
$I->see('Project Ideas', 'h1');
$I->see('Search', 'h2');

// Check that all Ideas show by default
$I->see('Ideas', 'h2');
$I->see('Idea 1', 'h3');
$I->see('Idea 2', 'h3');
$I->see('Idea 3', 'h3');

// Submit a Search Query and Check Result

//Select Web Development Field
$I->checkOption('#webDevelopment');
$I->click('Search', 'button');
// Should display only Idea 3
$I->see('Idea 3', 'h3');
// and no the others
$I->dontSee('Idea 1', 'h3');
$I->dontSee('Idea 2', 'h3');
