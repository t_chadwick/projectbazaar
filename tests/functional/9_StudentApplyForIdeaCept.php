<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Apply my interest in a Project Idea.');

// Test as PStudent User (Who has not applied)
$I->amLoggedAs(['email' => 'rbank@gmail.com', 'password' => 'iamrob']);

//Start on User Dashboard

$I->amOnPage('/ideas');
$I->see('Idea 3');
$I->see('Find Out More', 'a[href="/ideas/3"]');
$I->click('Find Out More', 'a[href="/ideas/3"]');

$I->seeCurrentUrlEquals('/ideas/3');
$I->see('Idea 3', 'h1');

$I->see('Apply', 'button');
$I->click('Apply', 'button');
$I->seeCurrentUrlEquals('/home');
$I->see('Idea 3', 'h3');
$I->see('Status: Pending');
