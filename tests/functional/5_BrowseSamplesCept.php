<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Browse through the Project Samples');

//-------------------------First as an Admin
$I->SeeRecord('users', [
      'email' => 'tedwards@gmail.com',
  ]);


// Check for Samples Records
$I->SeeRecord('samples', [
      'title' => 'Literature Review Sample 1',
]);
$I->SeeRecord('samples', [
      'title' => 'Methodology Section Sample 1',
]);
$I->SeeRecord('samples', [
      'title' => 'Gannt Chart Sample 1',
]);

$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// Starts at Dashboard
$I->amOnPage('/home');
$I->see('Dashboard');
$I->see('Project Samples');
$I->click('Project Samples');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/samples');
$I->see('Project Samples');

// Check for known samples in list
$I->see('Literature Review Sample 1', 'h3');
$I->see('Take a look at this sample to see how a literature review should be written.', 'p');
$I->seeLink('Take a Look','/samples/show/1');
$I->see('Methodology Section Sample 1', 'h3');
$I->see('Take a look at this sample to see how you should document your project methodology.', 'p');
$I->seeLink('Take a Look','/samples/show/2');
$I->see('Gannt Chart Sample 1', 'h3');
$I->see('A Gannt chart can be useful in planning out your project.', 'p');
$I->seeLink('Take a Look','/samples/show/3');
