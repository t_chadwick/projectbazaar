<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Browse through the Projex Articles');

//-------------------------First as an Admin
$I->SeeRecord('users', [
      'email' => 'tedwards@gmail.com',
]);

// Check for articles records
$I->SeeRecord('articles', [
      'title' => 'First Article!',
]);
$I->SeeRecord('articles', [
      'title' => 'Second Article!',
]);
$I->SeeRecord('articles', [
      'title' => 'Final Article!',
]);

$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// Starts on Dashboard
$I->amOnPage('/home');
$I->see('Dashboard');
$I->see('Projex Blog');
$I->click('Projex Blog');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/projex');
$I->see('Welcome to Projex');

// Check that articles can be seen in List
$I->see('Articles', 'h2');
$I->see('First Article', 'h3');
$I->see('This Article has been created as part of Development. It is the first of 3 mock posts.', 'p');
$I->seeLink('Read More','/projex/show/1');
$I->see('Second Article', 'h3');
$I->see('As with the first article this has been made for development.', 'p');
$I->seeLink('Read More','/projex/show/2');
$I->see('Final Article', 'h3');
$I->see('Last of the 3 test articles.', 'p');
$I->seeLink('Read More','/projex/show/3');
