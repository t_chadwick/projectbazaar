<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Find all Articles within the Research Category');

// Test as Admin User (Full Access)
$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// On Projex Index Page
$I->amOnPage('/projex');
$I->see('Welcome to Projex', 'h1');
$I->see('Search', 'h2');

// Check that all Articles show by default
$I->see('Articles', 'h2');
$I->see('First Article', 'h3');
$I->see('Second Article', 'h3');
$I->see('Final Article', 'h3');

// Submit a Search Query and Check Result

//Select 'Research Category'
$I->checkOption('input[value=Research]');
$I->click('Search', 'button');
// Should only display 'Second Article'
$I->see('Second Article', 'h3');
// and NOT the other two
$I->dontSee('First Article', 'h3');
$I->dontSee('Final Article', 'h3');
