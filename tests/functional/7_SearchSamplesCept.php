<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Test the Search Feature for Project Samples');

// Test as Admin User (Full Access)
$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// On Samples Index Page
$I->amOnPage('/samples');
$I->see('Project Samples', 'h1');
$I->see('Search', 'h2');

// Check that all Ideas show by default
$I->see('Samples', 'h2');
$I->see('Literature Review Sample 1', 'h3');
$I->see('Methodology Section Sample 1', 'h3');
$I->see('Gannt Chart Sample 1', 'h3');

// Submit 4 Search Queries and Check Results

// Blank Search
$I->fillField('kwSearch', '');
$I->click('Search', 'button');
// Should display all Samples
$I->see('Literature Review Sample 1', 'h3');
$I->see('Methodology Section Sample 1', 'h3');
$I->see('Gannt Chart Sample 1', 'h3');

//'Lit Review' Search
$I->fillField('kwSearch', 'Lit Review');
$I->click('Search', 'button');
// Should only display 'Literature Review Sample 1'
$I->see('Literature Review Sample 1', 'h3');
// and NOT the other two
$I->dontSee('Methodology Section Sample 1', 'h3');
$I->dontSee('Gannt Chart Sample 1', 'h3');

//'Method' Search
$I->fillField('kwSearch', 'Method');
$I->click('Search', 'button');
// Should only display 'Methodology Section Sample 1'
$I->see('Methodology Section Sample 1', 'h3');
// and NOT the other two
$I->dontSee('Literature Review Sample 1', 'h3');
$I->dontSee('Gannt Chart Sample 1', 'h3');

//'Gannt Chart' Search
$I->fillField('kwSearch', 'Gannt Chart');
$I->click('Search', 'button');
// Should only display 'Gannt Chart Sample 1'
$I->see('Gannt Chart Sample 1', 'h3');
// and NOT the other two
$I->dontSee('Literature Review Sample 1', 'h3');
$I->dontSee('Methodology Section Sample 1', 'h3');
