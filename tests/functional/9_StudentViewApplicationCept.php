<?php
$I = new FunctionalTester($scenario);
$I->wantTo('View my idea application to check its status.');

// Test as PStudent User
$I->amLoggedAs(['email' => 'ccross@gmail.com', 'password' => 'password']);

//Start on User Dashboard

$I->amOnPage('/home');
$I->see('Your Applications', 'h2');
$I->see('Idea 2', 'h3');
$I->see('Status: Accepted');
