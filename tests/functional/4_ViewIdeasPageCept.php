<?php

// This Test Includes 4 Different Scenarios for each User Type.
//  -- They CANNOT be run together, for one to be run the others
//     must be commented out.
// This has been done and they each pass.

$I = new FunctionalTester($scenario);
$I->wantTo('View the Ideas Page');


//-------------------------First as an Admin
$I->SeeRecord('users', [
      'email' => 'tedwards@gmail.com',
  ]);

$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// Starts on Dashboard
$I->amOnPage('/home');
$I->see('Dashboard');
$I->see('Project Ideas');
$I->click('Project Ideas');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/ideas');
$I->see('Project Ideas');


/*
//----------------------------Then as a Tutor
$I->SeeRecord('users', [
      'email' => 'hhighwater@gmail.com',
  ]);

$I->amLoggedAs(['email' => 'hhighwater@gmail.com', 'password' => 'password']);

// Starts on Dashboard
$I->amOnPage('/home');
$I->see('Dashboard');
$I->see('Project Ideas');
$I->click('Project Ideas');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/ideas');
$I->see('Project Ideas');




//----------------Finally as a Project Student
$I->SeeRecord('users', [
      'email' => 'rbank@gmail.com',
  ]);

$I->amLoggedAs(['email' => 'rbank@gmail.com', 'password' => 'iamrob']);

// Starts on Dashboard
$I->amOnPage('/home');
$I->see('Dashboard');
$I->see('Project Ideas');
$I->click('Project Ideas');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/ideas');
$I->see('Project Ideas');
*/
