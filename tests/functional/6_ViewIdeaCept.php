<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Find and View a Single Project Idea');

//-------------------------As an Admin
$I->SeeRecord('users', [
      'email' => 'tedwards@gmail.com',
  ]);

// Check for Idea Record


// I start on Landing Page
$I->amOnPage('/');
$I->see('Project Bazaar');

// I Click Login
$I->click('Login');

// Which takes me to the Login page
$I->seeCurrentUrlEquals('/login');
$I->see('Login');

//Fills in Login Form
$I->fillField('email', 'tedwards@gmail.com');
$I->fillField('password', 'iamadmin');
$I->click('Login', 'button');
// Gets to Dashboard
$I->seeCurrentUrlEquals('/home');
$I->see('Dashboard');
$I->see('Project Ideas');
$I->click('Project Ideas');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/ideas');
$I->see('Project Ideas');

// Find the Idea and Click on it
$I->see('Idea 1', 'h3');
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
$I->seeLink('Find Out More','/ideas/1');
$I->click('Find Out More');

//Gets to Idea page
$I->seeCurrentUrlEquals('/ideas/1');
$I->see('Idea 1', 'h1');
// Check for the Sample Description
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
// Check for other Elements
$I->see('Apply', 'button');
$I->see('Helen Highwater');
$I->see('hhighwater@gmail.com');
