<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Find and View a Single Projex Article');

//-------------------------As an Admin
$I->SeeRecord('users', [
      'email' => 'tedwards@gmail.com',
  ]);

// Check for Article Record


// I start on Landing Page
$I->amOnPage('/');
$I->see('Project Bazaar');

// I Click Login
$I->click('Login');

// Which takes me to the Login page
$I->seeCurrentUrlEquals('/login');
$I->see('Login');

//Fills in Login Form
$I->fillField('email', 'tedwards@gmail.com');
$I->fillField('password', 'iamadmin');
$I->click('Login', 'button');
// Gets to Dashboard
$I->seeCurrentUrlEquals('/home');
$I->see('Dashboard');
$I->see('Projex Blog');
$I->click('Projex Blog');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/projex');
$I->see('Welcome to Projex');

// Find the Article in the list and Click on it
$I->see('Articles', 'h2');
$I->see('First Article', 'h3');
$I->see('This Article has been created as part of Development. It is the first of 3 mock posts.', 'p');
$I->seeLink('Read More','/projex/show/1');
$I->click('Read More');

// Gets to Article page
$I->seeCurrentUrlEquals('/projex/show/1');
$I->see('First Article!', 'h1');
// Check for the different article sections
$I->see('Section 1: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'p');
$I->see('Section 2: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'p');
