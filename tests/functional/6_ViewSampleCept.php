<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Find and View a single Project Sample');

//-------------------------First as an Admin
$I->SeeRecord('users', [
      'email' => 'tedwards@gmail.com',
  ]);


// Check for Sample Record


// I start on Landing Page
$I->amOnPage('/');
$I->see('Project Bazaar');

// I Click Login
$I->click('Login');

// Which takes me to the Login page
$I->seeCurrentUrlEquals('/login');
$I->see('Login');

//Fills in Login Form
$I->fillField('email', 'tedwards@gmail.com');
$I->fillField('password', 'iamadmin');
$I->click('Login', 'button');
// Gets to Dashboard
$I->seeCurrentUrlEquals('/home');
$I->see('Dashboard');
$I->see('Project Samples');
$I->click('Project Samples');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/samples');
$I->see('Project Samples');

// Find the Sample and Click on it
$I->see('Literature Review Sample 1', 'h3');
$I->see('Take a look at this sample to see how a literature review should be written.', 'p');
$I->seeLink('Take a Look','/samples/show/1');
$I->click('Take a Look');

//Gets to Sample page
$I->seeCurrentUrlEquals('/samples/show/1');
$I->see('Literature Review Sample 1', 'h1');
// Check for the Sample Description
$I->see('Take a look at this sample to see how a literature review should be written.', 'p');
// Check for the actual Sample Content
$I->seeElement('img', ['class' => 'sample-img']);
