<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Update an Existing Idea.');

// Test as Tutor User
$I->amLoggedAs(['email' => 'wpeace@gmail.com', 'password' => 'longstory']);

//Start on User Dashboard

$I->amOnPage('/home');
$I->see('Your Project Ideas', 'h2');
$I->see('Idea 2', 'h3');
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
$I->click('Manage', 'a[href="/ideas/2/edit"]');

$I->seeCurrentUrlEquals('/ideas/2/edit');

$I->seeInField(['name' => 'title'], 'Idea 2');
// Error found with this check, it cannot get the value of a text area in the usual way, looking at https://github.com/Codeception/Codeception/issues/2464 it seems that an id may need to be implemented
 // $I->see('Explain what the idea is, what needs to be done and what the end result should be.Explain what the idea is, what needs to be done and what the end result should be.', '#desc');

$I->see('Save Changes', 'button');

$I->fillField('title', 'Idea 2 Test');

// Another error has come from this line, again it seems that an id needs to be used  https://laracasts.com/discuss/channels/general-discussion/codeception-unreachable-field-description?page=1

// Applying the id does not fix the error
$I->fillField('#desc', 'This description has been edited as part of a test.');
$I->click('Save Changes');

$I->seeCurrentUrlEquals('/ideas/2/edit');
$I->seeInField(['name' => 'title'], 'Idea 2 Test');
$I->see(['name' => 'desc'], 'This description has been edited as part of a test.');


// Could not get past the error, tested the process in browser and ideas can be created.

// PASSED
