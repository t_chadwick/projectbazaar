<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Create a New Idea.');

// Test as Tutor User
$I->amLoggedAs(['email' => 'wpeace@gmail.com', 'password' => 'longstory']);

//Start on User Dashboard

$I->amOnPage('/home');
$I->see('Your Project Ideas', 'h2');
$I->seeLink('New Idea', '/ideas/create');
$I->click('New Idea', 'a[href="/ideas/create"]');

$I->seeCurrentUrlEquals('/ideas/create');
$I->see('Create New Project Idea');
$I->see('Title', 'Label');
$I->seeElement('input', ['name' => 'title']);
$I->fillField('title', 'Test Idea');
$I->see('Description', 'Label');
$I->seeElement('textarea', ['name' => 'desc']);
$I->fillField('desc', 'This Idea has been created as part of a test.');

$I->see('Field', 'Label');
$I->seeElement('input', ['name' => 'field'], ['value' => 'Web Development']);
$I->seeElement('input', ['name' => 'field'], ['value' => 'Games Development']);
$I->seeElement('input', ['name' => 'field'], ['value' => 'Computing']);
$I->selectOption('form input[name=field]', 'Computing');

$I->see('Project Type', 'Label');
$I->seeElement('input', ['name' => 'type'], ['value' => 'Research']);
$I->seeElement('input', ['name' => 'type'], ['value' => 'Development']);
$I->selectOption('form input[name=type]', 'Development');

$I->see('Maximum Applications', 'Label');
$I->seeElement('input', ['name' => 'max_applications']);
$I->fillField('max_applications', '3');

$I->seeElement('button', ['name' => 'create'], ['type' => 'submit']);
$I->click('Create', 'button');

$I->seeCurrentUrlEquals('/home');

$I->see('Test Idea', 'h3');
$I->see('This Idea has been created as part of a test.', 'p');
