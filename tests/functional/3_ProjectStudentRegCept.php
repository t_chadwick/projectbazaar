<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Register as a Project Student');

// I start on Landing Page
$I->amOnPage('/');
$I->see('Project Bazaar');
$I->click('Register');

$I->dontSeeRecord('users', [
      'fname' => 'Test2',
      'sname' => 'User2',
      'email' => 'tester2@gmail.com',
      'role' => 'PStudent',
  ]);

$I->amOnPage('/register');
$I->see('Register');

// I Fill in Registration Form
$I->fillField('fname', 'Test2');
$I->fillField('sname', 'User2');
$I->fillField('email', 'tester2@gmail.com');
$I->fillField('password', 'testing');
$I->fillField('password_confirmation', 'testing');
$I->fillField('role', 'PStudent');
$I->click('Register', 'button');
// Gets to Dashboard
$I->seeCurrentUrlEquals('/home');
$I->see('Dashboard');
