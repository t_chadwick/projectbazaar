<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Test the Search Feature for Articles');

// Test as Admin User (Full Access)
$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// On Projex Index Page
$I->amOnPage('/projex');
$I->see('Welcome to Projex', 'h1');
$I->see('Search', 'h2');

// Check that all Articles show by default
$I->see('Articles', 'h2');
$I->see('First Article', 'h3');
$I->see('Second Article', 'h3');
$I->see('Final Article', 'h3');

// Submit 4 Search Queries and Check Results

// Blank Search
$I->fillField('kwSearch', '');
$I->click('Search', 'button');
// Should display all articles
$I->see('First Article', 'h3');
$I->see('Second Article', 'h3');
$I->see('Final Article', 'h3');

//'First' Search
$I->fillField('kwSearch', 'First');
$I->click('Search', 'button');
// Should only display 'First Article'
$I->see('First Article', 'h3');
// and NOT the other two
$I->dontSee('Second Article', 'h3');
$I->dontSee('Final Article', 'h3');

//'S' Search
$I->fillField('kwSearch', 's');
$I->click('Search', 'button');
// Should only display 'First Article' and 'Second Article'
$I->see('First Article', 'h3');
$I->See('Second Article', 'h3');
// and NOT the other one
$I->dontSee('Final Article', 'h3');

//'Final Article!' Search
$I->fillField('kwSearch', 'Final Article!');
$I->click('Search', 'button');
// Should only display 'Final Article'
$I->see('Final Article', 'h3');
// and NOT the other two
$I->dontSee('First Article', 'h3');
$I->dontSee('Second Article', 'h3');
