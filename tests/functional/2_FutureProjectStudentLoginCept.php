<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Login as a Future Project Student');

$I->SeeRecord('users', [
      'email' => 'rkarrs@gmail.com',
  ]);

// I start on Landing Page
$I->amOnPage('/');
$I->see('Project Bazaar');

// I Click Login
$I->click('Login');

// Which takes me to the Login page
$I->seeCurrentUrlEquals('/login');
$I->see('Login');

//Fills in Login Form
$I->fillField('email', 'rkarrs@gmail.com');
$I->fillField('password', 'ilikecars');
$I->click('Login', 'button');
// Gets to Dashboard
$I->seeCurrentUrlEquals('/home');
$I->see('Dashboard');
