<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Browse through Project Ideas');

//-------------------------As an Admin
$I->SeeRecord('users', [
      'email' => 'tedwards@gmail.com',
]);

// Check for Ideas Records
$I->SeeRecord('ideas', [
      'title' => 'Idea 1',
]);
$I->SeeRecord('ideas', [
      'title' => 'Idea 2',
]);
$I->SeeRecord('ideas', [
      'title' => 'Idea 3',
]);

$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// Starts at Dashboard
$I->amOnPage('/home');
$I->see('Dashboard');
$I->see('Project Ideas');
$I->click('Project Ideas');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/ideas');
$I->see('Project Ideas');

// Check for known ideas in list
$I->see('Idea 1', 'h3');
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
$I->seeLink('Find Out More','/ideas/1');
$I->see('Idea 2', 'h3');
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
$I->seeLink('Find Out More','/ideas/2');
$I->see('Idea 3', 'h3');
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
$I->seeLink('Find Out More','/ideas/3');
