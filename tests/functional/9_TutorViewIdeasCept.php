<?php
$I = new FunctionalTester($scenario);
$I->wantTo('View My Project Ideas');

// Test as Tutor User
$I->amLoggedAs(['email' => 'wpeace@gmail.com', 'password' => 'longstory']);

//Start on User Dashboard

$I->amOnPage('/home');
$I->see('Your Project Ideas', 'h2');
$I->see('Idea 2', 'h3');
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
$I->seeLink('View', '/ideas/2');
$I->seeLink('Manage', '/ideas/2/edit');
$I->see('Idea 3', 'h3');
$I->see('Explain what the idea is, what needs to be done and what the end result should be.', 'p');
$I->seeLink('View', '/ideas/3');
$I->seeLink('Manage', '/ideas/3/edit');
$I->click('View', 'a[href="/ideas/3"]');

$I->seeCurrentUrlEquals('/ideas/3');

$I->see('Idea 3');
