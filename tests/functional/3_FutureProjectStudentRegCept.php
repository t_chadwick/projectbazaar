<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Register as a Future Project Student');

// I start on Landing Page
$I->amOnPage('/');
$I->see('Project Bazaar');
$I->click('Register');

$I->dontSeeRecord('users', [
      'fname' => 'Test3',
      'sname' => 'User3',
      'email' => 'tester3@gmail.com',
      'role_id' => '4',
  ]);

$I->amOnPage('/register');
$I->see('Register');

// I Fill in Registration Form
$I->fillField('fname', 'Test3');
$I->fillField('sname', 'User3');
$I->fillField('email', 'tester3@gmail.com');
$I->fillField('password', 'testing');
$I->fillField('password_confirmation', 'testing');
$I->selectOption('role', '4');
$I->click('Register', 'button');
// Gets to Dashboard
$I->seeCurrentUrlEquals('/home');
$I->see('Dashboard');
