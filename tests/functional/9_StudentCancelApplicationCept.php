<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Cancel my Application.');

// Test as PStudent User
$I->amLoggedAs(['email' => 'pester@gmail.com', 'password' => 'prettypolly']);

//Start on User Dashboard

$I->amOnPage('/home');
$I->see('Your Applications', 'h2');
$I->see('Idea 1', 'h3');
$I->see('Status: Pending');
$I->see('Cancel Application', 'button');
$I->click('Cancel Application', 'button');

$I->seeCurrentUrlEquals('/home'); //Page reloads
$I->dontSee('Idea 1');
