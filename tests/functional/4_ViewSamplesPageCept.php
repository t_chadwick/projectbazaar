<?php

// This Test Includes 4 Different Scenarios for each User Type.
//  -- They CANNOT be run together, for one to be run the others
//     must be commented out.
// This has been done and they each pass.

$I = new FunctionalTester($scenario);
$I->wantTo('View the Samples Page');

//-------------------------First as an Admin
$I->SeeRecord('users', [
      'email' => 'tedwards@gmail.com',
  ]);

$I->amLoggedAs(['email' => 'tedwards@gmail.com', 'password' => 'iamadmin']);

// Starts on Dashboard
$I->amOnPage('/home');
$I->see('Dashboard');
$I->see('Project Samples');
$I->click('Project Samples');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/samples');
$I->see('Project Samples');

/*
//----------------------------Then as a Tutor
$I->SeeRecord('users', [
      'email' => 'hhighwater@gmail.com',
  ]);

$I->amLoggedAs(['email' => 'hhighwater@gmail.com', 'password' => 'password']);

// Starts on Dashboard
$I->amOnPage('/home');
$I->see('Dashboard');
$I->see('Project Samples');
$I->click('Project Samples');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/samples');
$I->see('Project Samples');

//----------------Finally as a Project Student
$I->SeeRecord('users', [
      'email' => 'rbank@gmail.com',
  ]);

$I->amLoggedAs(['email' => 'rbank@gmail.com', 'password' => 'iamrob']);

// Starts on Dashboard
$I->amOnPage('/home');
$I->see('Dashboard');
$I->see('Project Samples');
$I->click('Project Samples');
// I Go to the Projex Blog Homepage
$I->seeCurrentUrlEquals('/samples');
$I->see('Project Samples');
*/
