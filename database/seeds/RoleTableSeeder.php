<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert([
          ['id' => 1,'name' => 'Admin'],
          ['id' => 2,'name' => 'Tutor'],
          ['id' => 3,'name' => 'PStudent'],
          ['id' => 4,'name' => 'FPStudent'],
      ]);
    }
}
