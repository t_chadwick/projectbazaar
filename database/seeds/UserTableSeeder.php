<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
            ['id' => 1,'fname' => 'Rob A.', 'sname' => 'Bank',
             'email' => 'rbank@go.edgehill.ac.uk', 'password' => bcrypt('password'), 'role_id' => 3,
            ],
            ['id' => 2,'fname' => 'Rex', 'sname' => 'Karrs',
             'email' => 'rkarrs@go.edgehill.ac.uk', 'password' => bcrypt('password'), 'role_id' => 4,
            ],
            ['id' => 3,'fname' => 'Helen', 'sname' => 'Highwater',
             'email' => 'hhighwater@edgehill.ac.uk', 'password' => bcrypt('password'), 'role_id' => 2,
            ],
            ['id' => 4,'fname' => 'Ted', 'sname' => 'Edwards',
             'email' => 'tedwards@edgehill.ac.uk', 'password' => bcrypt('iamadmin'), 'role_id' => 1,
            ],
            ['id' => 5,'fname' => 'Chris', 'sname' => 'Cross',
             'email' => 'ccross@go.edgehill.ac.uk', 'password' => bcrypt('password'), 'role_id' => 3,
            ],
            ['id' => 6,'fname' => 'Polly', 'sname' => 'Ester',
             'email' => 'pester@go.edgehill.ac.uk', 'password' => bcrypt('password'), 'role_id' => 3,
            ],
            ['id' => 7,'fname' => 'Warren', 'sname' => 'Peace',
             'email' => 'wpeace@edgehill.ac.uk', 'password' => bcrypt('password'), 'role_id' => 2,
            ],
        ]);
    }
}
