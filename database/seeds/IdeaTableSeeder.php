<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class IdeaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ideas')->insert([
            ['id' => 1,'title' => 'Idea 1', 'desc' => 'Explain what the idea is, what needs to be done and what the end result should be.',
             'category_id' => 4, 'type' => 'Development', 'applications' => 1, 'max_applications' => 5, 'user_id' => 3,  'created_at' => Carbon::now()->addWeeks(3)->format('Y-m-d H:i:s'),
            ],
            ['id' => 2,'title' => 'Idea 2', 'desc' => 'Explain what the idea is, what needs to be done and what the end result should be.',
             'category_id' => 5, 'type' => 'Research', 'applications' => 1, 'max_applications' => 3, 'user_id' => 7,  'created_at' => Carbon::now()->addWeeks(4)->format('Y-m-d H:i:s'),
            ],
            ['id' => 3,'title' => 'Idea 3', 'desc' => 'Explain what the idea is, what needs to be done and what the end result should be.',
             'category_id' => 6, 'type' => 'Development', 'applications' => 0, 'max_applications' => 2, 'user_id' => 7,  'created_at' => Carbon::now()->addWeeks(5)->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
