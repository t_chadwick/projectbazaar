<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SampleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('samples')->insert([
            ['id' => 1,'title' => 'Literature Review Sample 1', 'desc' => 'Take a look at this sample to see how a literature review should be written.',
             'category_id' => 7, 'fps_view' => 1,  'created_at' => Carbon::now()->addWeeks(1)->format('Y-m-d H:i:s'),
            ],
            ['id' => 2,'title' => 'Methodology Section Sample 1', 'desc' => 'Take a look at this sample to see how you should document your project methodology.',
             'category_id' => 7, 'fps_view' => 0,  'created_at' => Carbon::now()->addWeeks(3)->format('Y-m-d H:i:s'),
            ],
            ['id' => 3,'title' => 'Gannt Chart Sample 1', 'desc' => 'A Gannt chart can be useful in planning out your project.',
             'category_id' => 1, 'fps_view' => 0,  'created_at' => Carbon::now()->addWeeks(4)->format('Y-m-d H:i:s'),
            ],
            ['id' => 4,'title' => 'Proposal Sample', 'desc' => 'This proposal sample should not be visisble to Future Project Students',
             'category_id' => 7, 'fps_view' => 0,  'created_at' => Carbon::now()->addWeeks(2)->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
