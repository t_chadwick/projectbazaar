<?php

use Illuminate\Database\Seeder;

class ApplicationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('applications')->insert([
            ['id' => 1,'status' => 'Pending','user_id' => 6, 'idea_id' => 1,],
            ['id' => 2,'status' => 'Accepted','user_id' => 5, 'idea_id' => 2,],
        ]);
    }
}
