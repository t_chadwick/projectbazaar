<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            ['id' => 1,'title' => 'First Article!', 'desc' => 'This Article has been created as part of Development. It is the first of 3 mock posts.', 'author' => 'Admin', 'category_id' => 1, 'created_at' => Carbon::now()->addWeeks(1)->format('Y-m-d H:i:s'),
            ],
            ['id' => 2,'title' => 'Second Article!', 'desc' => 'As with the first article this has been made for development.',
             'author' => 'Admin', 'category_id' => 2,
             'created_at' => Carbon::now()->addWeeks(2)->format('Y-m-d H:i:s'),
            ],
            ['id' => 3,'title' => 'Final Article!', 'desc' => 'Last of the 3 test articles.',
             'author' => 'Admin', 'category_id' => 3,
             'created_at' => Carbon::now()->addWeeks(3)->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
