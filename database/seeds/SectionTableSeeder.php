<?php

use Illuminate\Database\Seeder;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->insert([
            ['id' => 1,
            'content' => 'Section 1: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
             'article_id' => 1,],
             ['id' => 2,
             'content' => 'Section 2: Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
              'article_id' => 1,],
              ['id' => 3,
              'content' => 'This section is for the second article! This section is for the second article! This section is for the second article!',
               'article_id' => 2,],
               ['id' => 4,
               'content' => 'This section is for the second article! This section is for the second article! This section is for the second article!',
                'article_id' => 2,],
                ['id' => 5,
                'content' => 'This section is for the second article! This section is for the second article! This section is for the second article!',
                 'article_id' => 2,],
                 ['id' => 6,
                 'content' => 'This is the only section for this article!',
                  'article_id' => 3,],
        ]);
    }
}
