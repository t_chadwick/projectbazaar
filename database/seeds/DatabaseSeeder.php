<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(ArticleTableSeeder::class);
        $this->call(IdeaTableSeeder::class);
        $this->call(ApplicationTableSeeder::class);
        $this->call(SectionTableSeeder::class);
        $this->call(SampleTableSeeder::class);
    }
}
