<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert([
          ['id' => 1,'name' => 'Time Management'],
          ['id' => 2,'name' => 'Research'],
          ['id' => 3,'name' => 'Productivity'],
          ['id' => 4,'name' => 'Computing'],
          ['id' => 5,'name' => 'Game Development'],
          ['id' => 6,'name' => 'Web Development'],
          ['id' => 7,'name' => 'Writing'],
      ]);
    }
}
