## Origins
Project Bazaar is a php based web application built using the Laravel5.4 framework. I developed it as part of my Final Year Project at university. The application aims to provide guidance to students when undertaking their own projects as well as allow Tutors to advertise project ideas to students.


## So far...
Seen as this is for my dissertation, I will not be working on it any further for some time after the deadline. But after a while I may return and further develop it.
So far the following has been implemented:

- Projex Blog:
  - Articles can be created by Admin and read by Users.
  - At the moment only a few sample articles have been added.
- Project Samples:
 - Samples can be added by Admin and viewed by Users. At the moment these consist of a description and an image which has to be coded in. (Not ideal, See Roadmap)
 - As with the articles a few samples have been added.
- Project Ideas:
 - Tutors can post projet ideas for Students to view.
 - Students can apply for project Ideas.
 - Tutors can manage Ideas and their Applications.

## Project Roadmap
Further Development of this application would involve:

- Samples Uploads by Admin as images or documents not hard coded.
- More personal for user. E.g. Bookmark/favourite articles or Suggested Articles.
- Interactive Samples.
- Tutor-Student communication about Project Ideas.
- More content. E.g. Actual Articles and Samples.

## Installation

To try out the application, download or clone this repository. Then follow these instructions. If you are familiar with Laravel deployment then this should be straight forward.

### Npm and Composer
Run the following commands within the project directory.
- $ npm install
- $ composer install

### Database

- Make a Copy of '.env.example' and rename it to '.env'.

- Create a MySQL Database Schema with utf-8 default collation.

- Edit the following lines in '.env':

     1. DB_DATABASE='YOUR_DATABASE_NAME'
     1. DB_USERNAME='YOUR_USERNAME' (or 'root')
     1. DB_PASSWORD='YOUR USER PASSWORD'


- In Console, Run:

 - $ php artisan key:generate
 - $ php artisan migrate
 - $ php artisan db:seed

 You can confirm all of this has worked by:

 - Checking '.env'. It should have an Application Key on line 2.
 - Checking your database. It should now be populated with tables and each table should have some seeded data.

### Start the Server

 - $ php artisan serve

 You should be given a URL where the application is being served. By default, it will be http://localhost:8000.





## Use

With the application up and running, you can now use the following user accounts to access it.

### User Accounts
The following table contains the predefined user accounts included within the Seed Data. Note: The accounts are not real, they have been made for testing purposes.

| User Name | Email | Password | User Role |
| -------------   | ------------------------- | ---------| ---------------------- |
| Ted Edwards     | tedwards@edgehill.ac.uk   | iamadmin | Admin                  |
| Warren Peace    | wpeace@edgehill.ac.uk     | password | Tutor                  |
| Helen Highwater | hhighwater@edgehill.ac.uk | password | Tutor                  |
| Rob A. Bank     | rbank@go.edgehill.ac.uk   | password | Project Student        |
| Chris Cross     | ccross@go.edgehill.ac.uk  | password | Project Student        |
| Polly Ester     | pester@go.edgehill.ac.uk  | password | Project Student        |
| Rex Karrs       | rkarrs@go.edgehill.ac.uk  | password | Future Project Student |

## Feedback

Found a bug? Got a suggestion?

Email me at: tom@tchadwickdev.co.uk