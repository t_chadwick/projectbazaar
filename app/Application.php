<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    //Belongs to One User
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //Belongs to One Idea
    public function idea()
    {
        return $this->belongsTo('App\Idea');
    }
}
