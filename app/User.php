<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'sname', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Relationships

    //Has Many Ideas
    public function ideas()
    {
        return $this->hasMany('App\Idea');
    }
    //Can only Make one application
    public function applications()
    {
        return $this->belongsTo('App\Application');
    }

    //Belongs to One Role
    public function role()
    {
        return $this->belongsTo('App\Role');
    }


}
