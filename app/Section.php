<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    //Belongs to One Article
    public function article()
    {
        return $this->belongsTo('App\Article');
    }
}
