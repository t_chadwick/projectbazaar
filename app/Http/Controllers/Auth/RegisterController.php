<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Validation\Rule;
use Request;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function index () {
      return view('auth/register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        echo "Validating User";
        return Validator::make($data, [
            'fname' => 'required|max:255',
            'sname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'role_id' => [
                'required',
                Rule::in([1, 2, 3, 4]),
            ],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
      echo "Creating User";
      return User::create([
                  'fname' => $data['fname'],
                  'sname' => $data['sname'],
                  'email' => $data['email'],
                  'password' => bcrypt($data['password']),
                  'role_id' => $data['role'],
              ]);
    }

    protected function store(Request $request) {
      // Get the Data
      $fname = Request::get('fname');
      $sname = Request::get('sname');
      $email = Request::get('email');
      $password = Request::get('password');
      $password2 = Request::get('password_confirmation');
      $role = Request::get('role');

      // Validate the data
      if (gettype($fname) == 'string' && gettype($sname) == 'string' && filter_var($email, FILTER_VALIDATE_EMAIL) && $password === $password2 && $role <= 4 && $role >= 2) {
        // If the Data Types are Valid, check the email address (edgehill domain)
        if(strrpos($email, '@go.edgehill.ac.uk') || sttrpos($email, '@edgehill.ac.uk')) {
          //Data Passes Validation, So register the User
          User::create([
                      'fname' => $fname,
                      'sname' => $sname,
                      'email' => $email,
                      'password' => bcrypt($password),
                      'role_id' => $role,
                  ]);
          return redirect('/login');
        } else {
          /*
           At these two points the Validation has failed and the user will be redirected to the Register Page. Ideally this needs to also return an error message indicating what they did wrong, this can be implemented later if time permits. It would require separating each check into the individual if statements where failing adds an error to a list which is then printed to the user.
           */
          return redirect('/register');
        }
      } else {
        return redirect('/register');
      }

    }
}
