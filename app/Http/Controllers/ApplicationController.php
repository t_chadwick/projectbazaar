<?php

namespace App\Http\Controllers;

use Auth;
use App\Application;
use App\Idea;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      // User Authentication
      $user = Auth::user();
      // Check for an Existing Application made by the User
      $check = Application::where('user_id', $user->id)->first();
      // If one Exists, redirect the User and cancel the application
      if($check) {
        return redirect('/home');
      }

      //Create an Application and Sync it to the Idea
        $user = Auth::user();
        $id = request('idea_id');
        $application=  new Application;

        $application->status = 'Pending';
        $application->user_id = $user->id;
        $application->idea_id = $id;


        $application->save();

        $idea = Idea::find($id);
        $idea->applications = $idea->applications + 1;
        $idea->save();


        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // Update Application to Accepted
      $application = Application::findOrFail($id);

      $application->status = 'Accepted';

      $application->save();

      $idea_id = $application->idea_id;
      return redirect('/ideas/'. $idea_id .'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find the Application and Delete it
        $idea_id = request('idea_id');
        $idea = Idea::find($idea_id);
        $idea->applications = $idea->applications - 1;
        $idea->save();

        $application = Application::find($id);
        $application->delete();
        return redirect('/home');
    }
}
