<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\Article;
use App\Section;
use Request;

use Illuminate\Support\Facades\Auth;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = Auth::user();
      $id = request('article_id');
      $article = Article::where('id', $id)->first();
      $section =  new Section;
      $section->article_id = $id;
      $section->content = ' ';
      $section->save();
      $sections = Section::all()->where('article_id', $id);
      return view('/admin/articles/edit', ['article' => $article], ['sections' => $sections])->with('user', $user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $section = Section::findOrFail($id);

      $section->content = Request::get('content');
      $article_id = Request::get('article_id');

      // Need to implement section updating
      $section->save();
      return redirect('/projex/'. $article_id .'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      //
      $section = Section::find($id);
      $section->delete();
      $article_id = request('article_id');
      return redirect('/projex/'. $article_id .'/edit');
    }
}
