<?php

namespace App\Http\Controllers;
use App\Sample;
use App\Category;

// use Illuminate\Http\Request;
use Request;
use Illuminate\Support\Facades\Auth;

class SamplesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = Auth::user();
      $allCategories = Category::all();

      if(isset($user)) {

      } else {

      }
      // If the User is Admin, direct to admin page with full CRUD access
      if ($user->role->name == 'Admin') {
        $allSamples = Sample::all();
        return view('/admin/samples/index', ['allSamples' => $allSamples, 'user' => $user, 'allCategories' => $allCategories]);
      }

      if ($user->role->name == 'FPStudent') {
        $samples = Sample::all()->where('fps_view', 1);

        return view('/samples/index', ['searchSamples' => $samples], ['user' => $user, 'allCategories' => $allCategories]);
      }
      else {
        $kwSearch = '';
        $kwSearch = \Request::get('kwSearch');
        $categories = \Request::get('categories');

        if ($kwSearch == null && $categories == null) {
          // Return all Samples
          $searchSamples = Sample::all();
        } else if ($kwSearch == null) {
          // Return Samples that have the selected Categories
          $searchSamples = Sample::whereIn('category_id', $categories)->get();
        } else if ($categories == null) {
          $searchValues = preg_split('/\s+/', $kwSearch, -1, PREG_SPLIT_NO_EMPTY);
          $searchSamples = Sample::where(function ($q) use ($searchValues) {
            foreach ($searchValues as $value) {
              $q->Where('title', 'like', "%{$value}%");
            }
          })->orderBy('id')->paginate();
        } else {
          $searchValues = preg_split('/\s+/', $kwSearch, -1, PREG_SPLIT_NO_EMPTY);
          $searchSamples = Sample::where(function ($q) use ($searchValues) {
            foreach ($searchValues as $value) {
              $q->Where('title', 'like', "%{$value}%")->orWhere('category', 'like', "%{$value}%");
            }
          })->whereIn('category', $categories)->orderBy('id')->paginate();
        }
      }

      // Apply filter (if any)
      if (Request::input('filter') == 'newest') {
        // Return samples sorted Newest First
        $searchSamples = $searchSamples->sortBy('created_at');

      } else if (Request::input('filter') == 'oldest') {
        // Return samples sorted Oldest First
         $searchSamples = $searchSamples->sortByDesc('created_at');
      }
      // Return all samples with no filter (in order of id)
      return view('/samples/index', ['searchSamples' => $searchSamples], ['user'=> $user, 'allCategories' => $allCategories]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $user = Auth::user();
      $categories = Category::all();
      return view('/admin/samples/create', ['user' => $user, 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = Auth::user();
      $sample=  new Sample;

      $sample->title = request('title');
      $sample->desc = request('desc');
      $sample->category_id = request('category');
      $sample->fps_view = request('fps_view');


      $sample->save();

      return redirect('/admin/samples/' . $sample->id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $sample = Sample::where('id', $id)->first();
        return view('samples.show', ['sample' => $sample], ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $user = Auth::user();
      $sample = Sample::where('id', $id)->first();
      $categories = Category::all();
      return view('/admin/samples/edit', ['sample' => $sample, 'user' => $user, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $sample = Sample::findOrFail($id);

      $sample->title = Request::get('title');
      $sample->desc = Request::get('desc');
      $sample->category_id = Request::get('category');
      $sample->fps_view = Request::get('fps_view');

      $sample->save();
      return redirect('/samples/'. $id .'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //
      $sample = Sample::find($id);
      $sample->delete();
      return redirect('/samples');
    }
}
