<?php

namespace App\Http\Controllers;
use App\Idea;
use App\Application;
use App\Category;
// use Illuminate\Http\Request;
use Request;
use Illuminate\Support\Facades\Auth;

class IdeasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // Authenticate the User
      $user = Auth::user();

      // Get all Categories for use in multiple areas
      $allCategories = Category::all();

      // If the User is Admin, direct to admin page with full CRUD access
      if ($user->role->name == 'Admin') {
        $allIdeas = Idea::all();
        return view('/admin/ideas/index', ['allIdeas' => $allIdeas, 'user' => $user]);
      }
      // If the User is FPStudent, redirect
      if ($user->role->name == 'FPStudent') {
        return redirect('/home');
      }
      else {
        // Collect any inputed Search/Filter Data and Act based on that
        $kwSearch = '';
        $kwSearch = \Request::get('kwSearch');
        $fields = \Request::get('categories');
        // If there is no data (first visit) get all records
        if ($kwSearch == null && $fields == null) {
          // Return all Samples
          $searchIdeas = Idea::all();
        // If there are only category inputs
        } else if ($kwSearch == null) {
          // Return Idas that are within the selected fields
          $searchIdeas = Idea::whereIn('category_id', $fields)->get();
        // If there is only keyword input
        } else if ($fields == null) {
          // Take each key word and query the database using them
          $searchValues = preg_split('/\s+/', $kwSearch, -1, PREG_SPLIT_NO_EMPTY);
          $searchIdeas = Idea::where(function ($q) use ($searchValues) {
            foreach ($searchValues as $value) {
              $q->Where('title', 'like', "%{$value}%");
            }
          })->orderBy('id')->paginate();
        } else {
          $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);
          $searchIdeas = Idea::where(function ($q) use ($searchValues) {
            foreach ($searchValues as $value) {
              $q->Where('title', 'like', "%{$value}%")->orWhere('field', 'like', "%{$value}%");
            }
          })->whereIn('field', $fields)->orderBy('id')->paginate();
        }
      }

      // Apply filter (if any)
      if (Request::input('filter') == 'newest') {
        // Return ideas sorted Newest First
        $searchIdeas = $searchIdeas->sortBy('created_at');

      } else if (Request::input('filter') == 'oldest') {
        // Return ideas sorted Oldest First
         $searchIdeas = $searchIdeas->sortByDesc('created_at');
      }
      // Return all ideas with no filter (in order of id)
      return view('/ideas/index', ['searchIdeas' => $searchIdeas], ['user' => $user, 'allCategories' => $allCategories]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $categories = Category::all();
        return view('/ideas/create', ['user' => $user, 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store New Idea in Database

        $user = Auth::user();
        $idea=  new Idea;

        $idea->title = request('title');
        $idea->desc = request('desc');
        $idea->category_id = request('category');
        $idea->type = request('type');
        $idea->applications = 0;
        $idea->max_applications = request('max_applications');
        $idea->user_id = $user->id;

        $idea->save();

        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $idea = Idea::where('id', $id)->first();
        return view('ideas.show', ['idea' => $idea], ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $user = Auth::user();
      $idea = Idea::where('id', $id)->first();
      $applications = Application::where('idea_id', $idea->id)->get();
      return view('ideas.edit', ['idea' =>$idea, 'user' => $user, 'applications' => $applications]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $idea = Idea::findOrFail($id);

      $idea->title = Request::get('title');
      $idea->desc = Request::get('desc');

      $idea->save();
      return redirect('/ideas/'. $id .'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //
      $idea = Idea::find($id);
      $idea->delete();
      return redirect('/home');
    }
}
