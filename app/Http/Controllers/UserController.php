<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;

use Illuminate\Support\Facades\Auth;

use App\User;
use App\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $allUsers = User::all();
      $user = Auth::user();
      return view('/admin/users/index', ['allUsers' => $allUsers, 'user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $user = Auth::user();
      return view('/admin/users/create')->with('user', $user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $user = Auth::user();
      $user2 = User::where('id', $id)->first();
      $roles = Role::all();
      return view('/admin/users.edit', ['user2' =>$user2, 'user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      echo "update";
      $user = User::findOrFail($id);

      $user->fname = Request::get('fname');
      $user->sname = Request::get('sname');
      $user->email = Request::get('email');
      $user->role_id = Request::get('role_id');

      print_r($request);

      $user->save();

      return redirect('/admin/users/' . $user->id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user = User::find($id);
      $user->delete();
      return redirect('/admin');
    }
}
