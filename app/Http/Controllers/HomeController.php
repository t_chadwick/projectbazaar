<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Idea;
use App\Application;
use App\Article;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //Authenticate the User
        $user = Auth::user();

        // Get the Latest Blog Post and Project Idea
        $latestPost = Article::orderBy('created_at', 'desc')->first();
        $latestIdea = Idea::orderBy('created_at', 'desc')->first();

        // Return different pages or data depending on the User Role
          if ($user->role->name == 'Admin') {
            // Admin Page
            return view('/admin/index', ['user' => $user]);
          } else if ($user->role->name == 'Tutor') {
            // Tutor Homepage
          $ideas = Idea::where('user_id', $user->id)->get();
          return view('home', ['user' => $user], ['ideas' => $ideas,  'latestPost' => $latestPost, 'latestIdea' => $latestIdea]);
        } else if  ($user->role->name == 'PStudent') {
          // PStudent Homepage
          $application = Application::where('user_id', $user->id)->get()->first();
          return view('home', ['user' => $user], ['application' => $application,  'latestPost' => $latestPost, 'latestIdea' => $latestIdea]);
        } else if($user->role->name == 'FPStudent') {
          // FPStudent Homepage
          return view('home', ['user' => $user,  'latestPost' => $latestPost]);
        } else {
          return view('home', ['latestPost' => $latestPost]);
        }
    }
}
