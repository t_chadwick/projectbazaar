<?php

namespace App\Http\Controllers;
use App\Article;
use App\Section;
use App\Category;

use Illuminate\Support\Facades\DB;
// use Illuminate\Http\Request;
use Request;

use Illuminate\Support\Facades\Auth;


class ProjexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $kwSearch = Request::get('kwSearch');
        $categories = Request::get('categories');
        $allCategories = Category::all();
        $currentArticles = Request::get('currentArticles'); // This is a string

        // If the User is Admin, direct to admin page with full CRUD access

        if(isset($user)) {
          if ($user->role->name == 'Admin') {
            $allArticles = Article::all();
            return view('/admin/articles/index', ['allArticles' => $allArticles, 'user' => $user]);
          }

          if ($kwSearch == null && $categories == null) {
            // Return all Articles
            $searchArticles = Article::all();
          } else if ($kwSearch == null) {
            // Return Articles that have a matching category out of those provided
            $searchArticles = Article::whereIn('category_id', $categories)->get();
            // return view('/projex/index', ['searchArticles' => $searchArticles], ['user' => $user, 'allCategories' => $allCategories]);
          } else if ($categories == null) {
            // Return Articles that match key word search (name)
            $searchValues = preg_split('/\s+/', $kwSearch, -1, PREG_SPLIT_NO_EMPTY);
            $searchArticles = Article::where(function ($q) use ($searchValues) {
              foreach ($searchValues as $value) {
                $q->Where('title', 'like', "%{$value}%");
              }
            })->orderBy('id')->paginate();
          } else {
            $searchValues = preg_split('/\s+/', $kwSearch, -1, PREG_SPLIT_NO_EMPTY);
            $searchArticles = Article::where(function ($q) use ($searchValues) {
              foreach ($searchValues as $value) {
                $q->Where('title', 'like', "%{$value}%");
              }
            })->whereIn('category_id', $categories)->orderBy('id')->paginate();

            // return view('/projex/index', ['searchArticles' => $searchArticles], ['user' => $user]);

          }

          $filter = '';
          if (Request::input('filter') == 'newest') {
             // Return articles sorted Newest First
             $filter = 'newest';
           } else if (Request::input('filter') == 'oldest') {
             // Return articles sorted Oldest First
              $filter = 'oldest';

           }
          // Call the filter function to sort the records accordingly
          $searchArticles = $this->filter($searchArticles, $filter);


          return view('/projex/index', ['searchArticles' => $searchArticles], ['user' => $user, 'allCategories' => $allCategories]);
        } else {
          $searchArticles = Article::all();
          return view('/projex/index', ['searchArticles' => $searchArticles], ['user' => $user, 'allCategories' => $allCategories]);
        }
    }

    // Filter the records depending on which option has been chosen
    public function filter($searchArticles, $filter) {
      if ($filter == 'newest') {
        $searchArticles = $searchArticles->sortBy('created_at');
      } else if ($filter == 'oldest') {
        $searchArticles = $searchArticles->sortByDesc('created_at');
      }

      return $searchArticles;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $user = Auth::user();
      $categories = Category::all();
      return view('/admin/articles/create', ['user' => $user, 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = Auth::user();
      $article=  new Article;

      $article->title = request('title');
      $article->desc = request('desc');
      $article->category_id = request('category');
      $article->author = 'Admin';


      $article->save();

      return redirect('/admin/articles/' . $article->id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = Auth::user();
      $article = Article::where('id', $id)->first();
      $sections = Section::all()->where('article_id', $id);
      return view('projex.show', ['article' => $article], ['sections' => $sections])->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $user = Auth::user();
      $article = Article::where('id', $id)->first();
      $sections = Section::all()->where('article_id', $id);
      return view('/admin/articles/edit', ['article' => $article], ['sections' => $sections])->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $article = Article::findOrFail($id);

      $article->title = Request::get('title');
      $article->desc = Request::get('desc');

      // Need to implement section updating
      $article->save();
      return redirect('/projex/'. $id .'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $article = Article::find($id);
      $article->delete();
      return redirect('/projex');
    }


}
