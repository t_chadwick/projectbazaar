<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  //Has Many Articles
  public function articles()
  {
    return $this->hasMany('App\Article');
  }

  //Has Many Samples
  public function samples()
  {
    return $this->hasMany('App\Sample');
  }

  //Has Many Ideas
  public function ideas()
  {
    return $this->hasMany('App\Idea');
  }
}
