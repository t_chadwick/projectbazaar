<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //Has Many Sections
    public function sections()
    {
        return $this->hasMany('App\Section');
    }

    //Belongs to One Category
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
