<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    // Has Many Applications
    public function applications()
    {
        return $this->hasMany('App\Application');
    }

    //Belongs to One User
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
        'title', 'desc', 'field', 'type', 'max_applications',
    ];

    //Belongs to One Category
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
