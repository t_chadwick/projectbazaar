<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sample extends Model
{
  //Belongs to One Category
  public function category()
  {
      return $this->belongsTo('App\Category');
  }
}
