<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::resource('/register', 'Auth\RegisterController');
Route::resource('/admin/articles', 'ProjexController');
Route::resource('/admin/samples', 'SamplesController');
Route::resource('/sections', 'SectionController');
Route::resource('/samples', 'SamplesController');
Route::resource('/admin/users', 'UserController');

Route::resource('/admin', 'AdminController');

Route::get('/', function () {
    return view('welcome');
});
//Route::get('/projex', 'ProjexController@index');
Route::resource('/projex', 'ProjexController');


// Route::get('/projex/show/{article}', 'ProjexController@show');


Route::resource('/ideas', 'IdeasController');
Route::resource('/application', 'ApplicationController');


// Route::post('/admin/articles/{id}/sections/create', 'SectionController@create');




Route::get('/home', 'HomeController@index');
